<?php
	$theme_name = 'oitavo-andar';
	include_once('includes/metatag.php');

?>

	<body id="search" itemscope itemtype="http://schema.org/WebPage">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all">
			<?php include_once('includes/header.php'); ?>

			<section id="all">
				<div class="normal-box">
					<div class="list-posts normal-box">

						<?php if ( have_posts() ) : ?>
							<h1 class="title-coalhand title-space"><?php _e( 'Resultados de busca para ', $theme_name ); ?> <span class="search-result"><?php the_search_query(); ?></span></h1>


							<?php while ( have_posts() ) : the_post() ?>
								<article role="article" class="bigger normal-post" itemscope itemtype="http://schema.org/BlogPosting">
									<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
										<div class="flag-all flag-blue">
											<?php
												// Getting the category/categories from this post
												$categories = get_the_category();
												$catname = '';
												$catlink = '';
												$array = array();
												if( $categories ) {
													foreach ($categories as $category) {
														$catname = $category->name;
														$catlink = get_category_link( $category->term_id );
														// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
														$array[] = $catname;
													}
												}
											?>

											<span><?php echo implode(', ' , $array) ?></span>
										</div>

										<figure>
											<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title() ?>" />
										</figure>

										<div class="info">
											<h1 itemprop="image" itemprop="headline"><?php the_title() ?></h1>
										</div>
									</a>
								</article>

								<?php if ( $post->post_type == 'post' ) { ?>
								<?php } ?>

								<?php if ( $post->post_type == 'post' ) { ?>
								<?php } ?>
							<?php endwhile; ?>

							<?php global $wp_query; $total_pages = $wp_query->max_num_pages; if ( $total_pages > 1 ) { ?>
								<div class="pagination">
									<div class="item">
										<?php next_posts_link(__( 'Próximos', $theme_name )) ?>
									</div>
									<div class="item">
										<?php previous_posts_link(__( '<div class="item">Anteriores</div>', $theme_name )) ?>
									</div>
								</div>
							<?php } ?>
							<?php else : ?>
								<div class="no-results">
									<h1>Ops :(</h1>
									<p>
										Sua busca não retornou nenhum resultado.
									</p>

									<p>
										Mas relax...respire fundo e tente novamente.
									</p>

									<?php include('includes/search_form.php'); ?>

								</div>
							<?php endif; ?>
					</div>

					<?php include_once('includes/sidebar.php'); ?>

				</div>

			</section>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>

	</body>
</html>
