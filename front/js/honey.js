;(function( window, document, $, undefined ) {
  'use strict';

  var honey = (function() {

    var $private = {};
    var $public = {};

    var form = $('#honey-form');
    var formMessages = $('.form-messages');

    $public.newForm = function() {
      form.submit(function(event) {
        var user_name = $('#user_name').val();
        var user_email = $('#user_email').val();
        var user_phone = $('#user_phone').val();
        var msg = 'Nome: ' + user_name + '\nE-mail: ' + user_email + '\nTelefone: ' + user_phone;

        $.ajax({
          type: "POST",
          url: "https://mandrillapp.com/api/1.0/messages/send.json",
          data: {
            'key': 'paYc0aPh1PLI7hKwMySrWw',
            'message': {
              'from_email': user_email,
              'from_name': user_name,
              'headers': {
                'Reply-To': user_email
              },
              'subject': 'Oitavo Andar - ' + user_name + ' quer um brownie',
              'text': msg,
              'to': [
                {
                  'email': 'dtropiani@outlook.com',
                  'name': 'Day T.',
                  'type': 'to'
                }]
            }
          },
          beforeSend: function(){
            $(".load").show();
          }
        })

        .done(function(response) {
          $(".load").hide();

          $(formMessages).removeClass('error');
          $(formMessages).addClass('success');

          // Vamos mandar a mensagem
          $(formMessages).text('UHUUL! Agora é só esperar que entramos em contato :)');

          form.trigger('reset');
        })

        .fail(function(response) {
          $(".load").hide();

          $(formMessages).removeClass('success');
          $(formMessages).addClass('error');

          // Dependendo do tipo de erro, a mensagem muda
          if ( data.response !== '' ) {
            $(formMessages).text( 'OPS! Você tem certeza de que preencheu todos os campos?' );
          } else {
            $(formMessages).text('OPS!! Parece que houve um problema em enviar a sua mensagem :( Tente de novo daqui alguns minutinhos.');
          }
        });

        return false; // prevent page refresh

      });
    }

    $public.sendHoneyForm = function() {
      $(form).submit(function(event) {
        event.preventDefault();

        var formData = $(form).serialize();

        $.ajax({
          type  : 'POST',
          url   : $(form).attr('action'),
          data  : formData,
          beforeSend: function(){
             $(".load").show();
          },
        })

        .done( function( response ) {
          $(".load").hide();
          // Tenhamos certeza de que o formMessages tem a class 'success'
          $(formMessages).removeClass('error');
          $(formMessages).addClass('success');

          // Vamos mandar a mensagem
          $(formMessages).text(response);

          // Limpando o formulário
          $(form).trigger('reset');
        })

        .fail( function( data ) {
          $(".load").hide();
          // Tenhamos certeza de que o formMessages tem a class 'error'
          $(formMessages).removeClass('success');
          $(formMessages).addClass('error');

          // Enviando a mensagem
          if ( data.response !== '' ) {
            $(formMessages).text( data.responseText );
          } else {
            $(formMessages).text( 'Oops! Aconteceu alguma coisa que não conseguimos enviar a sua mensagem.' );
          }

        });

      });
    }

    $public.showWant = function() {
      $('#whereis').on('click' , function() {
        $('body').css('overflow' , 'hidden');
        $('#honey-contact').fadeIn('fast');
      });
    }

    $private.outNow = function() {
      $('body').css('overflow' , 'auto');
      $('#honey-contact').fadeOut('fast');
    }

    $public.exitFull = function() {
      $('html').click(function (e) {
    		if (e.target.className == 'all-center') {

    			$private.outNow();

        } else if ( e.target.className == 'center' ) {
    		  $private.outNow();
    		}
    	});
    }

    $public.showInputs = function() {
      $("input[name=contact]").change(function(){
        if ( $(this).val() == 'E-mail' ) {
          $('#user_phone').val('00 000000000');
          $('#user_phone').hide('fast');
          $('#user_email').val('');
          $('#user_email').show('fast');
        } else {
          $('#user_email').val('noemail@noemail.com');
          $('#user_email').hide('fast');
          $('#user_phone').val('');
          $('#user_phone').show('fast');
        }
      });
    }

    return $public;
  })();

  // Global
  window.honey = honey;
  honey.showWant();
  //honey.sendHoneyForm();
  honey.newForm();
  honey.exitFull();
  honey.showInputs();

})( window, document, jQuery );
