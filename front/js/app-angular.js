var app = angular.module('eightApp', [

]);

app.controller('AppCtrl', function($scope, AppService){
  $scope.user = {};
  $scope.isLoading = false;
  $scope.feedback = '';
  $scope.send = function(form) {
    if (form.$valid) {
      $scope.isLoading = true;
      AppService.send($scope.user).then(

        function success(res){
          $scope.isLoading = false;
          console.log('Success');
          console.log(res);
          $scope.feedback = 'Sua mensagem foi enviada com sucesso :) Em breve a gente se fala!!';
          $scope.user = {};
          form.$setPristine();
        },

        function error(err){
          $scope.isLoading = false;
          console.log('Error');
          console.log(err);
          $scope.feedback = 'Oops!! Algo ruim aconteceu :( Tente novamente!!';
        }

      );
    }
  }
});

app.service('AppService', function($q, $http){
  var self = {
    'serializeObj': function (obj) {
        var result = [];

        for (var property in obj)
            result.push(decodeURIComponent(property) + "=" + decodeURIComponent(obj[property]));

        return result.join("&");
    },
    'send': function (form) {
        var d = $q.defer();

        var obj = self.serializeObj(form);

        $http({
            url: '/wp-content/themes/oitavo-andar/send.php',
            method: "POST",
            cache: false,
            data: obj,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            d.resolve(data);

        }).error(function (data, status, headers, config) {
            d.reject(data);
        });

        return d.promise;
    }
  };

  return self;
});