;(function( window, document, $, undefined ) {
  'use strict';

  var contact = (function() {

    var $private = {};
    var $public = {};

    var form = $('#form-contact');
    var formMessages = $('#form-messages');

    $public.newContact = function() {
      form.submit(function () {
        var user_name = $('#user_name').val();
        var user_mail = $('#user_email').val();
        var msg= $('#user_message').val();

        $.ajax(
          {
            type: "POST",
            url: "https://mandrillapp.com/api/1.0/messages/send.json",
            data: {
              'key': 'paYc0aPh1PLI7hKwMySrWw',
              'message': {
                'from_email': user_mail,
                'from_name': user_name,
                'headers': {
                  'Reply-To': user_name
                },
                'subject': 'Oitavo Andar - Contato de ' + user_name,
                'text': msg,
                'to': [
                  {
                    'email': 'dtropiani@outlook.com',
                    'name': 'Day T.',
                    'type': 'to'
                  }]
              }
            },
            beforeSend: function(){
              $(".load").show();
            }
          })

          .done(function(response) {
            $(".load").hide();

            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');

            $(formMessages).text('YAY!! A sua mensagem foi enviada, agora é só esperar :)');

            form.trigger('reset');
          })

          .fail(function(data) {

            $(".load").hide();

            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');

            // Enviando a mensagem
            if ( data.response !== '' ) {
              $(formMessages).text( 'OPS! Você tem certeza de que preencheu todos os campos?' );
            } else {
              $(formMessages).text( 'OPS! Aconteceu alguma coisa que não conseguimos enviar a sua mensagem.' );
            }

          });
        return false; // prevent page refresh
      });
    }

    $public.callContact = function() {

      $(form).submit( function( event ) {

        // Hey browser, stop submit it!
        event.preventDefault();

        // Serializando o form
        var formData = $(form).serialize();

        $.ajax({
          type  : 'POST',
          url   : $(form).attr('action'),
          data  : formData,
          beforeSend: function(){
             $(".load").show();
          },
        })

        .done( function( response ) {
          $(".load").hide();
          // Tenhamos certeza de que o formMessages tem a class 'success'
          $(formMessages).removeClass('error');
          $(formMessages).addClass('success');

          // Vamos mandar a mensagem
          $(formMessages).text(response);

          // Limpando o formulário
          $('#form-contact input').val('');
          $('#form-contact textarea').val('');
        })

        .fail( function( data ) {
          $(".load").hide();
          // Tenhamos certeza de que o formMessages tem a class 'error'
          $(formMessages).removeClass('success');
          $(formMessages).addClass('error');

          // Enviando a mensagem
          if ( data.response !== '' ) {
            $(formMessages).text( data.responseText );
          } else {
            $(formMessages).text( 'Oops! Aconteceu alguma coisa que não conseguimos enviar a sua mensagem.' );
          }

        });

      } );

    }

    return $public;
  })();

  // Global
  window.contact = contact;
  //contact.callContact();
  //contact.newContact();

})( window, document, jQuery );
