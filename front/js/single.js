;(function( window, document, $, undefined ) {
  'use strict';

  var single = (function() {

    var $private = {};
    var $public = {};

    $public.lightbox = function() {
      $('#post').find('img').parent().attr('data-lightbox' , 'Foto');
    }

    $public.placeholders = function() {
      $('#commentform').find('input#author').attr('placeholder','Nome*');
      $('#commentform').find('input#author').attr('required' , 'required');

      $('#commentform').find('input#email').attr('placeholder','E-mail*');
      $('#commentform').find('input#email').attr('type','email');
      $('#commentform').find('input#email').attr('required' , 'required');

      $('#commentform').find('textarea#comment').attr('placeholder','Comentário*');
      $('#commentform').find('textarea#comment').attr('required' , 'required');

      // Mostrando somente quando é carregado
      $('#commentform').fadeIn();
    }

    return $public;
  })();

  // Global
  window.single = single;
  single.lightbox();
  single.placeholders();

})( window, document, jQuery );
