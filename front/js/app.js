;(function( window, document, $, undefined ) {
  'use strict';

  var app = (function() {

    var $private = {};
    var $public = {};

    var cont = 0;
    var body_height = $('body').scrollTop();
    var screen_height = $(window).height();
    var $form = $('#mc-embedded-subscribe-form');
    var content;

    $private.register = function( $form ) {
      $.ajax({
        type : $form.attr('method'),
        url : $form.attr('action'),
        data : $form.serialize(),
        cache : false,
        dataType : 'json',
        contentType : "application/json; charset=utf-8",
        beforeSend: function(){
           $(".load").show();
        },
        error : function(err) {
          $(".load").hide();
          // alert('Could not connect to registration server. Please try again later.');
          // alert('Desculpe, mas não foi possível conectar com o servidor. Tente novamente em alguns minutos.');
          content = '<span>Desculpe, mas não foi possível conectar com o servidor. Tente novamente em alguns minutos.</span>'
          $('#main-footer .error').html( content );
        },
        success : function(data) {
          $(".load").hide();
          if ( data.result != "success" ) {
            // Something went wrong, do something to notify the user. maybe alert(data.msg);
            $('#main-footer .error').html( '' );
            $('#main-footer .success').html( '' );

            content = '<span>Ops! Alguma coisa deu errado. Tente de novo :)</span>'
            $('#main-footer .error').html( content );
          } else {
            // It worked, carry on...
            $('#main-footer .error').html( '' );
            $('#main-footer .success').html( '' );

            content = '<span>Yay! Confirme no seu e-mail.</span>'
            $('#main-footer .success').html( content );
          }
        }
      });
    }

    $private.validateInput = function( $form ) {
      // console.log( $form );
      return true;
    }

    $public.newsletterSubscribe = function() {
      $('#mc-embedded-subscribe').bind('click' , function( event ) {

        if ( event ) event.preventDefault();

        // validateInput = valida os campos do formulário
        if ( $private.validateInput ( $form ) ) {
          $private.register( $form );
        }

      });
    }

    $public.masks = function() {
      if ( $('body').hasClass('has-mask') ) {
        $(".phone").mask(" (99) 9999 - 9999?9 ");
      }
    }

    $public.scroll_effect = function() {
      $('.scroll').click(function(){
        $('html,body').animate({scrollTop: 0}, 500);
      });
    }

    $public.toTop = function() {
      var screen_width = $(window).height();

      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var topHeight = (screen_width);

        if (scroll >= topHeight) {
            $('.to-top').fadeIn();
            $('#top-header').fadeIn('fast');
        }

        if (scroll < topHeight) {
            $('.to-top').fadeOut();
            $('#top-header').fadeOut('fast');
        }
      });
    }

    $public.getInstagramPosts = function() {
      var urlJson = 'https://api.instagram.com/v1/users/220107606/media/recent/?access_token=220107606.09a08a4.6394f09497414aba8fae3483b037d3dd&count=8&callback=?';
      $.getJSON(urlJson, function(data) {
        var dt = data["data"];

        $.each(dt, function(val, v) {
          cont++;

          var url = v["images"]["standard_resolution"]["url"]; /*NOT STANDARD, and yes LOWER*/
          var cap = ''; // Variável que vai armazenar o valor de caption e que será utilizada como comparação

          // Se caption for nula, é atribuído um valor padrão pra ele
          if(v["caption"] == null) {
              cap = 'Está Convidado';
          } else {
              cap = v["caption"]["text"];
          }

          // Título da imagem
          var title = cap;

          // Link da imagem
          var link = v["link"];
          // Colocando as imagens na estrutura DOM
          $('<li class="'+ ( ((cont % 2) == 0) ? '' : 'insta-out' ) +' '+ ( (cont >= 9) ? 'insta-bye' : '' ) +' " />').append('<a href="'+link+'" title="'+title+'" target="_blank"><figure><img src="'+url+'" /></figure></a>').appendTo('.instagram-posts');
        });
      });

      $('#instagram').fadeIn('fast');

    }

  $public.getYouTube = function() {
    var json = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyAUNkM8KVY1sDTzAzK3mtaq4kVlvAMfRXY&channelId=UCIINukPYAsYdtb7D_TjGdfA&part=snippet,id&order=date&maxResults=1';

    $.getJSON(json, function(data){
      var t = data['items'];

      $.each(t, function(k, v){
        var id = v['id']['videoId'];
        $('#youtube-iframe').attr('src' , 'https://www.youtube.com/embed/' + id);

      });
    });

    $('.youtube-box').fadeIn();
  }

    $public.callSubmenu = function() {
      $('.has-sub').hover(function() {
        $(this).find('ul').stop(true,true).fadeIn('fast');
      }, function() {
        $(this).find('ul').stop(true,true).fadeOut('fast');
      });
    }

    $public.callResponsiveMenu = function() {
      $('#main-menu > ul').slicknav({
        label : '',
        prependTo:'#responsive-menu'
      });
    }

    $public.callSearch = function() {

      $('.click-search').on('click', function(){
        $('body').css('overflow' , 'hidden');
        $('.search-box').fadeIn('fast');
      });

    }

    // top-header also take this
    $public.closeSearch = function() {
      // if click outside of the div, it closes by itself
      $('html').click(function (e) {
    		if (e.target.className == 'all-center') {

    			$('body').css('overflow' , 'auto');
    			$('.search-box').fadeOut('fast');
    		}
    	});
    }

    $public.callTopHeader = function() {
      $('#top-header-content').click(function() {
        $('#top-header-content').fadeOut('fast');
        $('body').css('overflow' , 'auto');
      });

      $('.open-menu').on('click' , function() {
        $('#top-header-content').fadeIn('fast');
        $('body').css('overflow' , 'hidden');
      });
    }

    return $public;
  })();

  // Global
  window.app = app;
  app.callSubmenu();
  app.callResponsiveMenu();
  app.callTopHeader();
  app.callSearch();
  app.closeSearch();
  app.getInstagramPosts();
  app.getYouTube();
  app.scroll_effect();
  app.toTop();
  app.masks();
  app.newsletterSubscribe();

})( window, document, jQuery );
