
<!doctype html>

<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<meta name="verification" content="456bc0ebb5c30a23ba239dbba1b9ddfc" />

		<title>8º Andar - Por Day T.</title>
		<meta name="description" content="O Oitavo Andar é um blog que conta com a experiência de Day T. para contar sobre dicas de viagens no Brasil - e fora, de economia para essas viagens e muito mais!">
		<meta name="author" content="Day T.">
		<meta name="robots" content="index, nofollow">

		<link rel="shortcut icon" href="/wordpress/wp-content/themes/theme-name/favicon.ico">

		<!-- FACEBOOK
		<meta property="og:locale" content="pt_BR">
		<meta property="og:url" content="">
		<meta property="og:title" content="">
		<meta property="og:site_name" content="">
		<meta property="og:description" content="">
		<meta property="og:image" content="">
		<meta property="og:image:type" content="image/png">
		<meta property="og:image:width" content="=">
		<meta property="og:image:height" content="=">
		<meta property="og:type" content="website">
		-->

    <!-- Twitter Cards -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@youruser">
    <meta name="twitter:creator" content="Some title here">
    <meta name="twitter:title" content="Some title here - It could be the same">
    <meta name="twitter:description" content="An amazing description here.">
    <!-- <meta name="twitter:image:src" content="{{shareImage}}"> -->

		<link rel="stylesheet" type='text/css' href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" />
		<link rel="stylesheet" type='text/css' href="/wordpress/wp-content/themes/theme-name/style.css">
		<link rel="stylesheet" type='text/css' href="/wordpress/wp-content/themes/theme-name/assets/css/home.min.css">

		<!-- delete -->
		<link href='http://fonts.googleapis.com/css?family=Lato:100,300' rel='stylesheet' type='text/css'>
		<style type="text/css">
			* {
				padding: 0;
				margin: 0;
			}

			html, body {
				height: 100%;
			}

			body {
				text-align: center;
				display: flex;
				flex-direction: column;
				justify-content: center;
				font: 20px 'Lato', arial;
			}

			h1 {
				font-size: 70px;
				margin: 0;
			}

			h3,
			footer p {
				font-size: 12px;
				text-transform: uppercase;
			}

			h3 {
				margin-top: 50px;
			}

			a {
				color: #66BBAA;
				text-decoration: none;

				transition: color 0.5s ease-out;
				-webkit-transition: color 0.5s ease-out;
				-moz-transition: color 0.5s ease-out;
				-o-transition: color 0.5s ease-out;
				-ms-transition: color 0.5s ease-out;
			}

			a:hover {
				color: #000;
			}

			footer {
				width: 100%;
				position: absolute;
				text-align: center;
				bottom: 10px;
			}

			footer p > span {
				background-image: url('http://www.contrast.fi/wp-content/themes/contrast15/images/icons.gif');
				background-size: 30px;
				display: inline-block;
				width: 30px;
				height: 30px;
				display: inline-block;
				vertical-align: middle;
			}
		</style>
		<!-- delete -->

		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv-printshiv.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body id="home" itemscope itemtype="http://schema.org/WebPage">

		<div class="all">


			<header itemscope itemtype="http://schema.org/WPHeader">

			</header>


			<main class="content" role="main">
				<h1 role="banner" itemprop="headline">Oitavo Andar</h1>
				<h2>Por <a itemprop="sameAs" href="http://twitter.com/dtropiani" title="Day T." target="_blank">Day T.</a></h2>
				<h3 itemprop="description">Em breve</h3>
			</main>

			<footer>
				<p role="content info">
					Feito com <span></span> por <a href="http://cauealmeida.com" title="Cauê Almeida" target="_blank">Cauê Almeida</a>
				</p>
			</footer>



		<footer itemscope itemtype="http://schema.org/WPFooter">

		</footer>
		</div>


		<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/SlickNav/1.0.2/jquery.slicknav.min.js"></script>
		<script type="text/javascript" src="/wordpress/wp-content/themes/theme-name/assets/js/app.min.js"></script>		<!-- js additional -->
		<script type="text/javascript" src="/wordpress/wp-content/themes/theme-name/assets/js/home.min.js"></script>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-64206709-2', 'auto');
		  ga('send', 'pageview');

		</script>
	</body>

</html>
