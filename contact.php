<?php
	/*
		Template name: Contact Page
	*/
?>

<?php include_once('includes/metatag.php'); ?>

	<body id="single" itemscope itemtype="http://schema.org/WebPage" class="single-page contact">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all" ng-app="eightApp" ng-controller="AppCtrl">
			<?php include_once('includes/header.php'); ?>

			<section id="all">
				<div class="normal-box">
					<div class="list-posts normal-box">

						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<article role="article" class="bigger normal" id="post" itemscope itemtype="http://schema.org/BlogPosting">
								<h1 class="title-chalk title-space"><?php the_title() ?></h1>

								<div class="post-content">
									<?php the_content() ?>

									<?php include_once('includes/contact_form.php'); ?>

								</div>
							</article>

						<?php endwhile; else: ?>
						<?php endif; ?>

						<?php include_once('includes/last.php'); ?>

					</div>

					<?php include_once('includes/sidebar.php'); ?>

				</div>

			</section>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
		<script type="text/javascript" src="<?php echo $path; ?>/assets/js/app-angular.min.js"></script>
		<script type="text/javascript" src="<?php echo $path; ?>/assets/js/contact.min.js"></script>
	</body>
</html>
