<?php include_once('includes/metatag.php'); ?>

	<body id="archive" itemscope itemtype="http://schema.org/WebPage">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all">
			<?php include_once('includes/header.php'); ?>

			<main class="content" role="banner">
				<div class="normal-box">

					<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
					<?php /* If this is a category archive */ if (is_category()) { ?>
						<h1 itemprop="name" class="title title-coalhand">Categoria <?php echo single_cat_title(); ?> </h1>
					<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
						<h1 itemprop="name" class="title title-coalhand">Postagens de <?php the_time('j \d\e F \d\e Y'); ?></h1>
					<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
						<h1 itemprop="name" class="title title-coalhand">Postagens de <?php the_time('F \d\e Y'); ?></h1>
					<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
						<h1 itemprop="name" class="title title-coalhand">Postagens de <?php the_time('Y'); ?></h1>
					<?php /* If this is an author archive */ } elseif (is_author()) { ?>
						<?php
				    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
				    ?>
					  <h1 itemprop="name" class="title title-coalhand">Postagens de <?php echo $curauth->display_name; ?></h1>
					<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
					    <h1 itemprop="name" class="title title-coalhand">Postagens do blog</h1>
					<?php } ?>

					<div class="posts">

						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<article itemscope itemtype="http://schema.org/BlogPosting">

								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<header class="flag-all flag-blue">
										<?php
											// Getting the category/categories from this post
											$categories = get_the_category();
											$catname = '';
											$catlink = '';
											$array = array();
											if( $categories ) {
												foreach ($categories as $category) {
													$catname = $category->name;
													$catlink = get_category_link( $category->term_id );
													// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
													$array[] = $catname;
												}
											}
										?>

										<span><?php echo implode(', ' , $array) ?></span>
									</header>

									<figure>
										<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title(); ?>" />
									</figure>

									<footer class="info">
										<h1 itemprop="headline" class="title-coalhand">
											<?php the_title(); ?>
										</h1>
									</footer>
								</a>

							</article>
						<?php endwhile?>

							<?php if (function_exists("pagination")) {
							  pagination($additional_loop->max_num_pages);
							} ?>

						<?php else: ?>
							<div class="artigo">
								<h2>Nada Encontrado</h2>
								<p>Erro 404</p>
								<p>Lamentamos mas não foram encontrados artigos.</p>
							</div>           
						<?php endif; ?>
					</div>

					<div class="normal box-sep-big">
						<span class="separator-big"></span>
					</div>

					<div class="normal author">
						<article role="article" itemscope itemtype="http://schema.org/Person">
							<div class="thumb">
								<figure itemprop="image">
									<?php echo get_avatar( 2, 175 ); /* id, resolution */ ?>
								</figure>
							</div>

							<div class="info">
								<h1 class="title-coalhand title-darkgray"><?php the_author_meta('display_name', 2); ?></h1>

								<h2><?php the_author_meta('description', 2); ?></h2>

								<ul class="socials">
									<li>
										<a itemprop="sameAs" href="<?php echo $twitter_url ?>" class="ico ico-tt" title="Oitavo Andar no Twitter" target="_blank"></a>
									</li>

									<li>
										<a itemprop="sameAs" href="<?php echo $facebook_url ?>" class="ico ico-fb" title="Oitavo Andar no Facebook" target="_blank"></a>
									</li>

									<li>
										<a itemprop="sameAs" href="<?php echo $instagram_url ?>" class="ico ico-instagram" title="Oitavo Andar no Instagram" target="_blank"></a>
									</li>

									<li>
										<a itemprop="sameAs" href="<?php echo $youtube_url ?>" class="ico ico-yt" title="Oitavo Andar no YouTube" target="_blank"></a>
									</li>
								</ul>

							</div>
						</article>
					</div>

				</div>
			</main>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>

	</body>
</html>
