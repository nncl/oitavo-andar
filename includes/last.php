<section class="related last">
	<h3 class="title-coalhand simple-title">Veja também...</h3>

		<?php query_posts('posts_per_page=2'.'&paged='.$paged);?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<article role="article" class="bigger normal-post" itemscope itemtype="http://schema.org/BlogPosting">
				<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
					<div class="flag-all flag-blue">
					<?php
						// Getting the category/categories from this post
						$categories = get_the_category();
						$catname = '';
						$catlink = '';
						$array = array();
						if( $categories ) {
							foreach ($categories as $category) {
								$catname = $category->name;
								$catlink = get_category_link( $category->term_id );
								// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
								$array[] = $catname;
							}
						}
					?>

						<span class="cat-link"><?php echo implode(', ' , $array) ?></span>
					</div>

					<figure>
						<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title() ?>" />
					</figure>

					<div class="info">
						<h1 itemprop="image" itemprop="headline"><?php the_title() ?></h1>
					</div>
				</a>
			</article>

		<?php endwhile; else: ?>
			<h2>Nada Encontrado</h2>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
</section>
