<?php

	$orig_post = $post;
	global $post;
	$categories = get_the_category($post->ID);

	if ($categories) {
		$category_ids = array();

		foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			$args=array(
			'category__in' => $category_ids,
			'post__not_in' => array($post->ID),
			'posts_per_page'=> 2,
			'caller_get_posts'=>1
			);

			$my_query = new wp_query( $args );

			if( $my_query->have_posts() ) {
				echo '<section class="related">';
				echo 	'<h3 class="title-coalhand simple-title">Talvez você goste de...</h3>';

				while( $my_query->have_posts() ) {
					$my_query->the_post();?>

					<article role="article" class="bigger normal-post" itemscope itemtype="http://schema.org/BlogPosting">
						<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
							<div class="flag-all flag-blue">
								<?php
									// Getting the category/categories from this post
									$categories = get_the_category();
									$catname = '';
									$catlink = '';
									$array = array();
									if( $categories ) {
										foreach ($categories as $category) {
											$catname = $category->name;
											$catlink = get_category_link( $category->term_id );
											// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
											$array[] = $catname;
										}
									}
								?>

								<span><?php echo implode(', ' , $array) ?></span>
							</div>

							<figure>
								<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title() ?>" />
							</figure>

							<div class="info">
								<h1 itemprop="image" itemprop="headline"><?php the_title() ?></h1>
							</div>
						</a>
					</article>

				<?php }
					echo '</section>';
					}
				}

				$post = $orig_post;
				wp_reset_query();

				?>
