<aside id="main-aside">

	<div class="item author-description">
		<article role="article">
			<a href="<?php the_author_meta('user_url', 2); ?>" title="<?php the_author_meta('display_name', 2); ?>" target="_blank">
				<figure>
					<?php echo get_avatar( 2, 340 ); /* id, resolution */ ?>
				</figure>
			</a>

			<h1 itemprop="author"><?php the_author_meta('display_name', 2); ?></h1>

			<p itemprop="description">
				<?php the_author_meta('description', 2); ?>
			</p>

			<ul class="socials">
				<?php
				$ca_author_twitter = get_option('ca_author_twitter');
				if (!empty($ca_author_twitter)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_author_twitter ?>" class="ico ico-tt" title="Day no Twitter" target="_blank"></a>
				</li>
				<?php endif ?>

				<?php
				$ca_author_fb = get_option('ca_author_fb');
				if (!empty($ca_author_fb)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_author_fb ?>" class="ico ico-fb" title="Day no Facebook" target="_blank"></a>
				</li>
				<?php endif ?>

				<?php
				$ca_author_instagram = get_option('ca_author_instagram');
				if (!empty($ca_author_instagram)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_author_instagram ?>" class="ico ico-instagram" title="Day no Instagram" target="_blank"></a>
				</li>
				<?php endif ?>

				<?php
				$ca_author_youtube = get_option('ca_author_youtube');
				if (!empty($ca_author_youtube)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_author_youtube ?>" class="ico ico-yt" title="Day no YouTube" target="_blank"></a>
				</li>
				<?php endif ?>
			</ul>

		</article>
	</div> <!-- item -->

	<div class="item youtube-box">
		<article role="article" itemscope itemtype="http://schema.org/BlogPosting">
			<header>
				<h1 itemprop="headline" class="title-chalk">YouTube</h1>
			</header>

			<iframe itemprop="video" allowfullscreen id="youtube-iframe"></iframe>

		</article>
	</div>

	<div class="item insta-posts">
		<article role="article" itemscope itemtype="http://schema.org/BlogPosting">
			<header>
				<h1 itemprop="headline" class="title-chalk">Day no Instagram</h1>
			</header>

			<ul class="instagram-posts"></ul>

		</article>
	</div>

	<div class="item-nobg comercial">
		<article role="article" itemscope itemtype="http://schema.org/Article">
			<?php if( function_exists('bxslider') ) bxslider('8-bepartner'); ?>
		</article>
	</div>

	<div class="item">
		<div class="fb-page"
			data-href="https://www.facebook.com/pages/Oitavo-Andar/1620353448244823?__mref=message_bubble"
			data-small-header="false"
			data-adapt-container-width="true"
			data-hide-cover="false"
			data-show-facepile="true"
			data-show-posts="false">
				<div class="fb-xfbml-parse-ignore">
					<blockquote cite="https://www.facebook.com/facebook">
						<a href="https://www.facebook.com/pages/Oitavo-Andar/1620353448244823?__mref=message_bubble">Oitavo Andar</a>
					</blockquote>
				</div>
			</div>
	</div>

	<div class="item-nobg comercial">
		<article role="article" itemscope itemtype="http://schema.org/Article">
			<?php if( function_exists('bxslider') ) bxslider('filiate-1'); ?>
		</article>
	</div>

</aside>
