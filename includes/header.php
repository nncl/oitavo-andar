<header id="top-header">
	<span class="open-menu">&#9776;</span>

	<ul class="socials">
		<?php
		$ca_twitter = get_option('ca_twitter');
		if (!empty($ca_twitter)): ?>
		<li>
			<a itemprop="sameAs" href="<?php echo $ca_twitter ?>" class="ico ico-twitter" title="Oitavo Andar no Twitter" target="_blank"></a>
		</li>
		<?php endif ?>

		<?php
		$ca_fb = get_option('ca_fb');
		if (!empty($ca_fb)): ?>
		<li>
			<a itemprop="sameAs" href="<?php echo $ca_fb ?>" class="ico ico-fb" title="Oitavo Andar no Facebook" target="_blank"></a>
		</li>
		<?php endif ?>

		<?php
		$ca_instagram = get_option('ca_instagram');
		if (!empty($ca_instagram)): ?>
		<li>
			<a itemprop="sameAs" href="<?php echo $ca_instagram ?>" class="ico ico-instagram" title="Oitavo Andar no Instagram" target="_blank"></a>
		</li>
		<?php endif ?>

		<?php
		$ca_youtube = get_option('ca_youtube');
		if (!empty($ca_youtube)): ?>
		<li>
			<a itemprop="sameAs" href="<?php echo $ca_youtube ?>" class="ico ico-youtube" title="Oitavo Andar no YouTube" target="_blank"></a>
		</li>
		<?php endif ?>
	</ul>
</header>

<section id="top-header-content">
	<div class="box-top">

		<div class="all-center">
			<div class="center">
				<nav role="navigation">


						<ul>
							<li>
								<a href="<?php echo $url ?>" title="Início">Home</a>
							</li>

							<li>
								<a href="<?php echo $reception ?>" title="Recepção">Recepção</a>
							</li>

							<li>
								<a href="<?php echo $floors ?>" title="Andares">Andares</a>

								<ul>
									<li>
										<a href="<?php echo $first ?>" title="1º andar">1º andar</a>
									</li>

									<li>
										<a href="<?php echo $second ?>" title="2º andar">2º andar</a>
									</li>

									<li>
										<a href="<?php echo $third ?>" title="3º andar">3º andar</a>
									</li>

									<li>
										<a href="<?php echo $fourth ?>" title="4º andar">4º andar</a>
									</li>

									<li>
										<a href="<?php echo $fifth ?>" title="5º andar">5º andar</a>
									</li>

									<li>
										<a href="<?php echo $sixth ?>" title="6º andar">6º andar</a>
									</li>

									<li>
										<a href="<?php echo $seventh ?>" title="7º andar">7º andar</a>
									</li>
								</ul>
							</li>

							<li>
								<a href="<?php echo $honey_bread ?>" title="Olha o Brownie aí gente">Olha o Brownie</a>
							</li>

							<li>
								<a href="<?php echo $contact ?>" title="Contato">Contato</a>
							</li>
						</ul>

				</nav>
			</div>
		</div>

	</div>
</section>

<header id="header-main" itemscope itemtype="http://schema.org/WPHeader">
	<div class="search-box">
		<div class="inside">
			<div class="all-center">
				<div class="center">
					<?php include_once('search_form.php'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="normal-box">
		<nav role="navigation">
			<ul id="social-header">
				<?php
				$ca_twitter = get_option('ca_twitter');
				if (!empty($ca_twitter)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_twitter ?>" class="ico ico-twitter" title="Oitavo Andar no Twitter" target="_blank"></a>
				</li>
				<?php endif ?>

				<?php
				$ca_fb = get_option('ca_fb');
				if (!empty($ca_fb)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_fb ?>" class="ico ico-fb" title="Oitavo Andar no Facebook" target="_blank"></a>
				</li>
				<?php endif ?>

				<?php
				$ca_instagram = get_option('ca_instagram');
				if (!empty($ca_instagram)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_instagram ?>" class="ico ico-instagram" title="Oitavo Andar no Instagram" target="_blank"></a>
				</li>
				<?php endif ?>

				<?php
				$ca_youtube = get_option('ca_youtube');
				if (!empty($ca_youtube)): ?>
				<li>
					<a itemprop="sameAs" href="<?php echo $ca_youtube ?>" class="ico ico-youtube" title="Oitavo Andar no YouTube" target="_blank"></a>
				</li>
				<?php endif ?>
			</ul>
		</nav>
	</div> <!-- normal-box -->

	<a href="<?php echo $url ?>" title="Oitavo Andar">
		<figure>
			<img itemprop="logo" src="<?php echo $path ?>/front/images/logo.png" alt="Oitavo Andar" class="logo" />
		</figure>
	</a>

	<div class="normal-box">
		<nav id="responsive-menu"></nav>

		<nav id="main-menu" role="navigation">
			<ul>
				<li>
					<span class="ico ico-home"></span>
					<a href="<?php echo $url ?>" title="Início">Home</a>
				</li>

				<li>
					<span class="ico ico-reception"></span>
					<a href="<?php echo $reception ?>" title="Recepção">Recepção</a>
				</li>

				<li class="has-sub">
					<span class="ico ico-floors"></span>
					<a href="<?php echo $floors ?>" title="Andares">Andares</a>

					<ul>
						<li>
							<span class="ico-1st"></span>
							<a href="<?php echo $first ?>" title="1º andar">1º andar</a>
						</li>

						<li>
							<span class="ico-2st"></span>
							<a href="<?php echo $second ?>" title="2º andar">2º andar</a>
						</li>

						<li>
							<span class="ico-3st"></span>
							<a href="<?php echo $third ?>" title="3º andar">3º andar</a>
						</li>

						<li>
							<span class="ico-4st"></span>
							<a href="<?php echo $fourth ?>" title="4º andar">4º andar</a>
						</li>

						<li>
							<span class="ico-5st"></span>
							<a href="<?php echo $fifth ?>" title="5º andar">5º andar</a>
						</li>

						<li>
							<span class="ico-6st"></span>
							<a href="<?php echo $sixth ?>" title="6º andar">6º andar</a>
						</li>

						<li>
							<span class="ico-7st"></span>
							<a href="<?php echo $seventh ?>" title="7º andar">7º andar</a>
						</li>
					</ul>
				</li>

				<li>
					<span class="ico ico-honey-bread"></span>
					<a href="<?php echo $honey_bread ?>" title="Olha o Brownie aí gente">Olha o Brownie</a>
				</li>

				<li>
					<span class="ico ico-contact"></span>
					<a href="<?php echo $contact ?>" title="Contato">Contato</a>
				</li>
			</ul>

			<div class="search">
				<button type="button" role="button" class="ico-search click-search"></button>
			</div>

		</nav>
	</div>

</header>

<div class="to-top scroll">
	<a title="Ir para o topo">
		<span class="ico ico-top"></span>
		<span class="title-chalk">Ir para o topo</span>
	</a>
</div>
