<?php require 'config.php'; ?>
<!doctype html>

<html lang="pt-br">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<meta name="verification" content="456bc0ebb5c30a23ba239dbba1b9ddfc" />

		<title><?php is_front_page() ? bloginfo('name') : wp_title($title . ' -'); ?></title>
		<meta name="description" content="<?php echo $description ?>">
		<meta name="author" content="<?php echo $author_description ?>">
		<meta name="robots" content="index, follow">
		<meta property="fb:app_id" content="1618093858455395" />

		<link rel="shortcut icon" href="<?php echo $path ?>/favicon.ico">

		<!-- FACEBOOK
		<meta property="og:locale" content="pt_BR">
		<meta property="og:url" content="">
		<meta property="og:title" content="">
		<meta property="og:site_name" content="">
		<meta property="og:description" content="">
		<meta property="og:image" content="">
		<meta property="og:image:type" content="image/png">
		<meta property="og:image:width" content="=">
		<meta property="og:image:height" content="=">
		<meta property="og:type" content="website">
		-->

    <!-- Twitter Cards -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?php echo $twitter_user ?>">
    <meta name="twitter:creator" content="<?php echo $twitter_description_creator; ?>">
    <meta name="twitter:title" content="<?php echo $title ?>">
    <meta name="twitter:description" content="<?php echo $twitter_description; ?>">
    <!-- <meta name="twitter:image:src" content="{{shareImage}}"> -->

		<link rel="stylesheet" type='text/css' href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" />
		<link rel="stylesheet" type='text/css' href="<?php echo $path ?>/style.min.css" />

		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv-printshiv.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
