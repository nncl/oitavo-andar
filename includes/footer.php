		<footer itemscope itemtype="http://schema.org/WPFooter" id="main-footer" class="normal">
			<section class="all-foot">
				<div class="normal-box">
					<div class="item">
						<h2>
							<span>Quer mais?</span>
							<span>Assine a newsletter</span>
						</h2>

						<form action="//cauealmeida.us9.list-manage.com/subscribe/post-json?u=3faf19b38a55eccb1224ce59e&amp;id=77a10a2e77&c=?" method="GET" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<input type="email" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="E-mail" required>
							<button type="submit" role="button" name="subscribe" id="mc-embedded-subscribe" class="button">
								<span class="ico-submit"></span>
							</button>
							<span class="load" style="display: none"></span>
						</form>

						<div class="error"></div>
						<div class="success"></div>

						<ul class="socials">
							<li>
								<a itemprop="sameAs" href="<?php echo $twitter_url ?>" class="ico ico-tt" title="Day no Twitter" target="_blank"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="<?php echo $facebook_url ?>" class="ico ico-fb" title="Day no Facebook" target="_blank"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="<?php echo $instagram_url ?>" class="ico ico-instagram" title="Day no Instagram" target="_blank"></a>
							</li>

							<li>
								<a itemprop="sameAs" href="<?php echo $youtube_url ?>" class="ico ico-yt" title="Day no YouTube" target="_blank"></a>
							</li>
						</ul>

					</div>

					<div class="item">
						<div class="fb-page"
							data-href="https://www.facebook.com/pages/Oitavo-Andar/1620353448244823?__mref=message_bubble"
							data-height="300"
							data-small-header="false"
							data-adapt-container-width="true"
							data-hide-cover="false"
							data-show-facepile="true"
							data-show-posts="true">
								<div class="fb-xfbml-parse-ignore">
									<blockquote cite="https://www.facebook.com/pages/Oitavo-Andar/1620353448244823?__mref=message_bubble">
										<a href="https://www.facebook.com/facebook">Oitavo Andar</a>
									</blockquote>
								</div>
							</div>
					</div>
				</div>
			</section>

			<section itemscope itemtype="http://Organization" id="by" class="normal">
				<div class="normal-box">
					<p role="content info">
						Feito com <span></span> por <a itemprop="url" href="http://cauealmeida.com" title="Cauê Almeida" target="_blank"><span itemprop="founder">Cauê Almeida</span></a>
					</p>
				</div>
			</section>

			<?php wp_footer(); ?>
		</footer>
