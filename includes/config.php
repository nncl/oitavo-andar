<?php
	$path = get_template_directory_uri();
	// $path = '/oitavo-andar';
	$url = '/';

	// Header - menu
	$home = '/';

	$instagram_url = 'http://instagram.com/dtropiani';
	$facebook_url = 'https://www.facebook.com/8andarblog';
	$twitter_url = 'http://twitter.com/dtropiani';
	$google_url = 'https://plus.google.com/102789447521909389771/posts';
	$youtube_url = 'https://www.youtube.com/channel/UCIINukPYAsYdtb7D_TjGdfA';

	$day_facebook_url = 'http://facebook.com/dtropiani';

	// Contato
	$email = '';
	$email_tester = ''; // Testar emails

	// Blog info
	$bloginfo = get_bloginfo( 'description' );
	// $title = get_bloginfo( 'name' );
	$title = 'Oitavo Andar';
	// $page_title = the_title();

	// Metatags
	$description = $bloginfo;
	$author_description = 'Dayara Tropiani, estudante de Tradução e louca por viagens.';

	// Twitter cards
	$twitter_user = '@dtropiani';
	$twitter_description_creator = 'Twitter creator description';
	$twitter_description = 'Twitter description';

	// Menus
	$all_posts = '/todos-os-posts/';
	$reception = '/o-que-e-o-oitavo-andar-e-quem-e-a-day/';
	$floors = '';
	$honey_bread = '/olha-o-brownie/';
	$contact = '/contato/';

	$menu_path = '/category/';

	$first = $menu_path . 'primeiro-andar';
	$second = $menu_path . 'segundo-andar';
	$third = $menu_path .'terceiro-andar';
	$fourth = $menu_path .'quarto-andar';
	$fifth = $menu_path .'quinto-andar';
	$sixth = $menu_path .'sexto-andar';
	$seventh = $menu_path .'setimo-andar';

?>
