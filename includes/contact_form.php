<section id="contact-form">

	<form id="form-contact" role="form"
        name="contactForm"
        novalidate="novalidate"
        ng-submit="send(contactForm)">
		<input type="text"
                ng-class="
                    {
                        'ca-has-error' : !contactForm.user_name.$valid && (!contactForm.$pristine || contactForm.$submitted)
                    }
                "
                ng-model="user.name"
                name="user_name" id="user_name" placeholder="Nome*" required />

        <input type="email"
                ng-class="
                    {
                        'ca-has-error' : !contactForm.user_email.$valid && (!contactForm.$pristine || contactForm.$submitted)
                    }
                "
                ng-model="user.email"
                name="user_email" id="user_email" placeholder="E-mail*" required />

		<input type="text"
                ng-class="
                    {
                        'ca-has-error' : !contactForm.user_subject.$valid && (!contactForm.$pristine || contactForm.$submitted)
                    }
                "
                ng-model="user.subject"
                name="user_subject" id="user_subject" placeholder="Assunto*" required />

		<textarea
                ng-class="
                    {
                        'ca-has-error' : !contactForm.user_message.$valid && (!contactForm.$pristine || contactForm.$submitted)
                    }
                "
                ng-model="user.message"
                name="user_message" id="user_message" placeholder="Mensagem*" required></textarea>


		<button type="submit" ng-show="!isLoading" class="title-multicolore" id="send">Enviar</button>

		<div id="form-messages" class="ca-feedback">
            {{feedback}}
        </div>
		<span class="load" ng-show="isLoading"></span>
	</form>

</section>
