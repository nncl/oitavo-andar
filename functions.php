<?php
/* WIDGETS */

if (function_exists('register_sidebar'))
{
    register_sidebar(array(
        'name'          => 'Sidebar',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ));
}

// Featured image
    add_theme_support( 'post-thumbnails' );

// Update the login image page
	function my_custom_login_logo() {
  	echo '<style type="text/css">
  		body.login div#login h1 a {

  			background-image: url(/wp-content/themes/oitavo-andar/front/images/logo.png);
  			width: 320px;
        height: 155px;
  			-webkit-background-size: 100%;
  			background-size: 100%;
        border-box: box-sizing;
  		}
  	</style>';
	}
// End login image

add_action('login_head', 'my_custom_login_logo');

// A link to your homepage on Login page
	function my_login_logo_url() {
	    return get_bloginfo( 'url' );
	}
	add_filter( 'login_headerurl', 'my_login_logo_url' );

	function my_login_logo_url_title() {
	    return 'Oitavo Andar';
	}
	add_filter( 'login_headertitle', 'my_login_logo_url_title' );
// End link

//Featured images
	// code...
//End Featured images

// taking pages out from search results
function SearchFilter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','SearchFilter');
// End taking pages out

// the excerpt length
  function custom_excerpt_length( $length ) {
  	return 100;
  }

  add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
// end the excerpt length

// pagination without any plugin
function pagination($pages = '', $range = 4)
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class=\"normal pagination\"><span>Página ".$paged." de ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; Primeira</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Anterior</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"page larger\">".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Última &raquo;</a>";
         echo "</div>\n";
     }
}

// OITAVO'S PANEL
// Fixing the editor's permission bug
$role_object = get_role( 'editor' );
$role_object->add_cap( 'manage_options' );
// End fix

// create custom plugin settings menu
add_action('admin_menu', 'oitavo_create_menu');

function oitavo_create_menu() {

  //create new top-level menu
  add_menu_page('OI Configurações', 'Oitavo Andar Configurações', 'editor', 'oitavo-andar-custom-page', 'oi_settings_page');

  //call register settings function
  add_action( 'admin_init', 'register_mysettings' );
}

function register_mysettings() {
  // Redes sociais
  register_setting( 'oi-settings-group', 'ca_twitter' );
  register_setting( 'oi-settings-group', 'ca_fb' );
  register_setting( 'oi-settings-group', 'ca_instagram' );
  register_setting( 'oi-settings-group', 'ca_youtube' );

  // Redes sociais do autor
  register_setting( 'oi-settings-group', 'ca_author_twitter' );
  register_setting( 'oi-settings-group', 'ca_author_fb' );
  register_setting( 'oi-settings-group', 'ca_author_instagram' );
  register_setting( 'oi-settings-group', 'ca_author_youtube' );
}

function oi_settings_page() { ?>
  <div class="wrap">
    <h2>Oitavo Andar Configurações</h2>

      <form method="post" action="options.php">
        <?php settings_fields( 'oi-settings-group' ); ?>
        <h3>Redes sociais</h3>

        <table class="form-table">

          <tr valign="top">
            <th scope="row">Twitter</th>
            <td>
              <input type="text" name="ca_twitter" value="<?php echo get_option('ca_twitter'); ?>" placeholder="Ex: http://twitter.com/meu_user" />
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">Facebook</th>
            <td>
              <input type="text" name="ca_fb" value="<?php echo get_option('ca_fb'); ?>" placeholder="Ex: http://facebook.com/meu_user" />
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">Instagram</th>
            <td>
              <input type="text" name="ca_instagram" value="<?php echo get_option('ca_instagram'); ?>" placeholder="Ex: http://instagram.com/meu_user" />
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">YouTube</th>
            <td>
              <input type="text" name="ca_youtube" value="<?php echo get_option('ca_youtube'); ?>" placeholder="Ex: http://youtube.com/meu_user" />
            </td>
          </tr>

        </table>

        <h3>Redes sociais do Autor</h3>

        <table class="form-table">

          <tr valign="top">
            <th scope="row">Twitter</th>
            <td>
              <input type="text" name="ca_author_twitter" value="<?php echo get_option('ca_author_twitter'); ?>" placeholder="Ex: http://twitter.com/meu_user" />
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">Facebook</th>
            <td>
              <input type="text" name="ca_author_fb" value="<?php echo get_option('ca_author_fb'); ?>" placeholder="Ex: http://facebook.com/meu_user" />
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">Instagram</th>
            <td>
              <input type="text" name="ca_author_instagram" value="<?php echo get_option('ca_author_instagram'); ?>" placeholder="Ex: http://instagram.com/meu_user" />
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">YouTube</th>
            <td>
              <input type="text" name="ca_author_youtube" value="<?php echo get_option('ca_author_youtube'); ?>" placeholder="Ex: http://youtube.com/meu_user" />
            </td>
          </tr>

        </table>

        <p class="submit">
          <input type="submit" class="button-primary" value="<?php _e('Salvar') ?>" />
        </p>
      </form>

      <!-- <span>Última edição feita por: <?php echo ca_get_last_author() ?></span> -->
  </div>
<?php
}

function ca_get_last_author() {
  // It shows who has edited for the last time
  // And it sends an e-mail for all registered users
  $name = "Test";
  $email = "email@email.com";
  return $name . ', e-mail: ' . $email . '.';
}

?>
