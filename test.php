<?php
	/*
		Template name: Test e-mail
	*/
?>

<?php include_once('includes/metatag.php'); ?>

	<body id="archive" itemscope itemtype="http://schema.org/WebPage">

        <form class="form-horizontal" id="contact_form">
            <fieldset>

                <!-- Form Name -->
                <legend>Contact Us</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name</label>
                    <div class="col-md-5">
                        <input id="name" name="name" type="text" placeholder="Enter your full name here" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Email</label>
                    <div class="col-md-5">
                        <input id="email" name="email" type="text" placeholder="Enter your email address here" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="msg">Message</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="msg" name="msg" cols="6" rows="6"></textarea>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-primary">Send Message</button>
                    </div>
                </div>

            </fieldset>
        </form>

        <span id="response"></span>

		<?php include_once('includes/script.php') ?>
		<script>
			$(function($)
			{
				$("#contact_form").submit(function()
				{
					var email = $("#email").val(); // get email field value
					var name = $("#name").val(); // get name field value
					var msg = $("#msg").val(); // get message field value
					$.ajax(
						{
							type: "POST",
							url: "https://mandrillapp.com/api/1.0/messages/send.json",
							data: {
								'key': 'u6q142m0VE9amASUCtBdWQ',
								'message': {
									'from_email': email,
									'from_name': name,
									'headers': {
										'Reply-To': email
									},
									'subject': 'Oitavo Andar - Contato de' + name,
									'text': msg,
									'to': [
										{
											'email': 'nothingpersonal2@hotmail.com',
											'name': 'Day T.',
											'type': 'to'
										}]
								}
							}
						})
						.done(function(response) {
							$('#response').text('Your message has been sent. Thank you!');
							$("#name").val(''); // reset field after successful submission
							$("#email").val(''); // reset field after successful submission
							$("#msg").val(''); // reset field after successful submission
						})
						.fail(function(response) {
							$('#response').text('Error sending message');
						});
					return false; // prevent page refresh
				});
			});
		</script>
	</body>
</html>
