<?php include_once('includes/metatag.php'); ?>

	<body id="home" itemscope itemtype="http://schema.org/WebPage">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all">
			<?php include_once('includes/header.php'); ?>

			<main class="content" role="banner">

				<div class="normal-box">
					<!-- exclude wp posts from loop : http://www.wpbeginner.com/wp-themes/how-to-exclude-latest-post-from-the-wordpress-post-loop/ -->
					<!-- For each div.item, we'll show from loop two posts, and inside of a loop, the if statement :  -->
					<?php $cont = 0; ?>
					<?php // if ( ($cont % 2) == 0 ) { ?>
						<!-- <span>bigger</span> -->
					<?php // } else { ?>
						<!-- <span>smaller</span> -->
					<?php // } ?>

					<header id="banner">
						<!-- loop -->

						<div class="item">

							<?php query_posts('posts_per_page=2'.'&paged='.$paged);?>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<?php if (( $cont % 2 ) == 0 ) { ?>
									<?php $cont++; ?>
									<article role="article" class="bigger" itemscope itemtype="http://schema.org/BlogPosting">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<div class="flag cat-blue">
												<?php
													// Getting the category/categories from this post
													$categories = get_the_category();
													$catname = '';
													$catlink = '';
													$array = array();
													if( $categories ) {
														foreach ($categories as $category) {
															$catname = $category->name;
															$catlink = get_category_link( $category->term_id );
															// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
															$array[] = $catname;
														}
													}
												?>

												<span><?php echo implode(', ' , $array) ?></span>
											</div>

											<figure>
												<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title(); ?>" />
											</figure>

											<div class="info">
												<h1 itemprop="headline"><?php the_title(); ?></h1>
											</div>
										</a>
									</article>

								<?php } else { ?>
									<?php $cont++; ?>
									<article role="article" class="smallest" itemscope itemtype="http://schema.org/BlogPosting">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<div class="flag cat-blue">
												<?php
													// Getting the category/categories from this post
													$categories = get_the_category();
													$catname = '';
													$catlink = '';
													$array = array();
													if( $categories ) {
														foreach ($categories as $category) {
															$catname = $category->name;
															$catlink = get_category_link( $category->term_id );
															// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
															$array[] = $catname;
														}
													}
												?>

												<span><?php echo implode(', ' , $array) ?></span>
											</div>

											<figure>
												<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title(); ?>" />
											</figure>

											<div class="info">
												<h1 itemprop="image" itemprop="headline"><?php the_title(); ?></h1>
											</div>
										</a>
									</article>
								<?php } ?>

							<?php endwhile; else: ?>
<!--								<h2>Desculpe, nada foi encontrado.</h2>-->
							<?php endif; ?>
							<?php wp_reset_query(); ?>

						</div>

						<div class="item">

							<?php query_posts('posts_per_page=2'.'&paged='.$paged.'&offset=2');?>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
								<?php if (( $cont % 2 ) == 0 ) { ?>
									<?php $cont++; ?>
									<article role="article" class="smallest" itemscope itemtype="http://schema.org/BlogPosting">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<div class="flag cat-blue">
												<?php
													// Getting the category/categories from this post
													$categories = get_the_category();
													$catname = '';
													$catlink = '';
													$array = array();
													if( $categories ) {
														foreach ($categories as $category) {
															$catname = $category->name;
															$catlink = get_category_link( $category->term_id );
															// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
															$array[] = $catname;
														}
													}
												?>

												<span><?php echo implode(', ' , $array) ?></span>
											</div>

											<figure>
												<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title(); ?>" />
											</figure>

											<div class="info">
												<h1 itemprop="headline"><?php the_title(); ?></h1>
											</div>
										</a>
									</article>

								<?php } else { ?>
									<?php $cont++; ?>
									<article role="article" class="bigger" itemscope itemtype="http://schema.org/BlogPosting">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<div class="flag cat-blue">
												<?php
													// Getting the category/categories from this post
													$categories = get_the_category();
													$catname = '';
													$catlink = '';
													$array = array();
													if( $categories ) {
														foreach ($categories as $category) {
															$catname = $category->name;
															$catlink = get_category_link( $category->term_id );
															// $array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
															$array[] = $catname;
														}
													}
												?>

												<span><?php echo implode(', ' , $array) ?></span>
											</div>

											<figure>
												<img itemprop="image" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title(); ?>" />
											</figure>

											<div class="info">
												<h1 itemprop="image" itemprop="headline"><?php the_title(); ?></h1>
											</div>
										</a>
									</article>
								<?php } ?>

							<?php endwhile; else: ?>
<!--								<h2>Desculpe, nada foi encontrado.</h2>-->
							<?php endif; ?>
							<?php wp_reset_query(); ?>

						</div>
						<!-- end loop -->
					</header> <!-- #banner -->

				</div> <!-- normal-box -->

			</main>

			<section id="all">
				<div class="normal-box">
					<div class="list-posts normal-box">
						<!-- loop -->
						<?php query_posts('posts_per_page=5'.'&paged='.$paged);?>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="post">
								<article role="article" itemscope itemtype="http://schema.org/BlogPosting">
									<header>
										<h1>
											<a itemprop="url" href="<?php the_permalink() ?>" title="<?php the_title() ?>">
												<span itemprop="headline"><?php the_title() ?></span>
											</a>
										</h1>

										<div class="author">
											<?php
												$author_id=$post->post_author;
											?>

											<figure>
												<?php echo get_avatar( $author_id, 40 ); /* id, resolution/width */ ?>
											</figure>

											<div class="info">
												<h2>Por <?php the_author_posts_link(); ?></h2>
												<span itemprop="dateCreated"><?php the_time('d') ?> &#149; <?php the_time('m') ?> &#149; <?php the_time('Y') ?> </span>
											</div>

										</div>

									</header>

									<div class="thumb">
										<figure>
											<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="<?php the_title() ?>" />
										</figure>

										<div class="flag">
										<?php
											// Getting the category/categories from this post
											$categories = get_the_category();
											$catname = '';
											$catlink = '';
											$array = array();
											if( $categories ) {
												foreach ($categories as $category) {
													$catname = $category->name;
													$catlink = get_category_link( $category->term_id );
													$array[] = '<a href="'.$catlink.'">'.$catname.'</a>';
												}
											}
										?>

											<span class="cat-link"><?php echo implode(', ' , $array) ?></span>
										</div>

									</div>

									<div class="description">
										<?php the_excerpt(); ?>
									</div>

									<footer>
										<a itemprop="url" href="<?php the_permalink() ?>" title="Leia mais" class="read-more">Leia mais</a>
									</footer>

								</article>
							</div>
							<?php endwhile; else: ?>
								<h2>Nada Encontrado</h2>
							<?php endif; ?>
							<?php wp_reset_query(); ?>
							<!-- End loop posts -->

						<!-- end loop -->

						<nav role="navigation" class="btn-navigation">
							<a href="<?php echo $all_posts ?>/todos-os-posts" title="Ver todos" class="btn-separator btn-all center-it">Ver todos</a>
						</nav>

					</div>

					<?php include_once('includes/sidebar.php'); ?>

					<section id="instagram" class="normal">
						<article role="article" itemscope itemtype="http://schema.org/Article">
							<header>
								<h1 class="title-chalk">
									<span class="ico ico-instagram insta-bg"></span>
									<span>Day no</span>
									<span>Instagram</span>
								</h1>
							</header>

							<ul class="instagram-posts"></ul>

						</article>
					</section>

				</div>

			</section>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>

	</body>
</html>
