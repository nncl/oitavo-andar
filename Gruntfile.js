module.exports = function(grunt) {
	'use strict';

	// configuração do projeto
	var gruntConfig = {

		pkg: grunt.file.readJSON('package.json'),

		uglify: {
	        options: {
	            mangle: false
	        },
			target: {
				files: {
					'assets/js/app.min.js': [
						'bower_components/slicknav/dist/jquery.slicknav.min.js',
						'front/js/app.js'
					],
					'assets/js/single.min.js': [
            'bower_components/lightbox/js/lightbox.js',
						'front/js/single.js'
					],
					'assets/js/honey.min.js': [
						'front/js/honey.js'
					],
					'assets/js/contact.min.js': [
						'front/js/contact.js'
					],
					'assets/js/app-angular.min.js': [
						'front/js/app-angular.js'
					],
				}
			}
		},

	    sass: {
	      dist: {
	        options: {
	          compass: true,
	          style: 'expanded'
	        },
	        files: {
	          'style.css': 'front/sass/app.scss',
	          'assets/css/home.css': 'front/sass/home.scss',
	          'assets/css/archive.css': 'front/sass/archive.scss',
	          'assets/css/search.css': 'front/sass/search.scss',
	          'assets/css/single.css': 'front/sass/single.scss',
	          'assets/css/404.css': 'front/sass/404.scss',
	          'assets/css/honey.css': 'front/sass/honey.scss',
	          'assets/css/contact.css': 'front/sass/contact.scss'
	        }
	      }
	    },

		cssmin: {
			target: {
				files: {
					'style.min.css': [
						'bower_components/slicknav/dist/slicknav.css',
            'bower_components/lightbox/css/lightbox.css',
						'assets/css/home.css',
						'assets/css/archive.css',
						'assets/css/search.css',
						'assets/css/single.css',
						'assets/css/404.css',
						'assets/css/honey.css',
						'assets/css/contact.css',
						'style.css'
					]
				}
			}
		},

        cachebreaker: {
            dev: {
                options: {
                    match: [
                    	'assets/js/app.min.js',
	                    'assets/js/single.min.js',
	                    'assets/js/honey.min.js',
	                    'assets/js/contact.min.js',
	                    'assets/js/app-angular.min.js',

                    	'assets/css/style.min.css',
                    ]
                },
                files: {
                    src: ['*.php']
                }
            }
        },

		/*cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'assets/css',
		      src: ['*.css', '!*.min.css'],
		      dest: 'assets/css',
		      ext: '.min.css'
		    }]
		  }
		}*/

	    watch : {
	      minsass : {
	        files : [
	          'front/sass/**/*'
	        ],

	        tasks : [ 'sass', 'cssmin' ]
	      },

	      minjs : {
	        files : [
	          'front/js/**/*'
	        ],

	        tasks : [ 'uglify' ]
	      }
	    } // watch

	};

	grunt.initConfig(gruntConfig);

  	// plugins
  	grunt.loadNpmTasks('grunt-contrib-uglify');
   	grunt.loadNpmTasks('grunt-contrib-sass');
  	grunt.loadNpmTasks('grunt-contrib-cssmin');
  	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks('grunt-cache-breaker');

	// tarefas
	grunt.registerTask('default', ['uglify', 'sass', 'cssmin']);
	grunt.registerTask('deploy', ['cachebreaker']);

  	// Tarefa para Watch
  	grunt.registerTask( 'w', [ 'watch' ] );
};
