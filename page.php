<?php include_once('includes/metatag.php'); ?>

	<body id="single" itemscope itemtype="http://schema.org/WebPage" class="single-page">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all">
			<?php include_once('includes/header.php'); ?>

			<section id="all">
				<div class="normal-box">
					<div class="list-posts normal-box">

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<article role="article" class="bigger normal" id="post" itemscope itemtype="http://schema.org/BlogPosting">
							<h1 class="title-chalk title-space"><?php the_title() ?></h1>

							<div class="post-content">
								<?php the_content() ?>
							</div>
						</article>

					<?php endwhile; else: ?>
					<?php endif; ?>

					</div>

					<?php include_once('includes/sidebar.php'); ?>

				</div>

			</section>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>
		<script type="text/javascript" src="<?php echo $path; ?>/assets/js/single.min.js"></script>
	</body>
</html>
