<?php
	/*
		Template name: Honey Bread
	*/
?>

<?php include_once('includes/metatag.php'); ?>

	<body id="honey" itemscope itemtype="http://schema.org/WebPage" class="has-mask">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all">
			<?php include_once('includes/header.php'); ?>

			<main class="content honey-banner" role="banner">
				<div class="all-center">
					<div class="center">
						<h1 itemprop="name">
							<span>Olha o Brownie</span>
							<span>aí gente...</span>
						</h1>
					</div>
				</div>
			</main>

			<article role="article" class="honey-first" itemscope itemtype="http://schema.org/Article">
				<div class="item item-right">
					<p itemprop="text">
						Se tu passou pela parte da recepção já sabe que eu tenho um bocado de sonhos mas um bolso vazio, né? Mas vou te contar outra coisa, eu também tenho uma mão boa pra cozinha e tive uma ideia!
					</p>

					<p itemprop="text">
						Aqui tu pode acompanhar essas ideias deliciosas e comprá-las também! Se tu for de SP a gente vai com muito amor e carinho te entregar, se não, o carteiro vai te entregar com muito amor e carinho também!
						Vem fazer parte da nossa história.
					</p>
				</div>
			</article>

			<article class="honey-third" role="article" itemscope itemtype="http://schema.org/Article">
				<div class="item item-right">
					<p>
						<span>Então se você gostou, está com vontade de apreciar,</span>
						<span>experimentar, degustar, se você ficou com água na boca com todo </span>
						<span>esse chocolate <strong class="title-multicolore title-pattern-blue">BELGIS</strong>, clica aqui em baixo que saberemos o que </span>
						<span>fazer.</span>
					</p>

					<button type="button" id="whereis">Tô com fome AGORA!</button>
				</div>
			</article>

			<section id="honey-contact">
				<article role="article">
					<div class="all-center">
						<div class="center">
							<h3 class="title-coalhand">Só preencha essas informações que o resto a gente faz!!</h3>

							<div class="box-hcontact">
								<form id="honey-form">
									<span class="load"></span>
									<div class="item">
										<input type="text" name="user_name" placeholder="Digite o seu nome" required id="user_name" />

										<span class="title-multicolore title-pattern-blue">Como deseja ser contatado?</span>
										<div class="options">
											<input type="radio" name="contact" value="E-mail" id="mail" required="" />
											<label for="mail" class="title-multicolore">E-mail</label>
										</div>

										<div class="options">
											<input type="radio" name="contact" value="Telefone" id="phone" required />
											<label for="phone" class="title-multicolore">Telefone</label>
										</div>

										<div class="options answer">
											<input type="email" name="user_email" placeholder="Digite o seu e-mail" id="user_email" />
											<input type="text" name="user_phone" placeholder="Digite o seu telefone" id="user_phone" class="phone" />
										</div>

										<button type="submit" class="title-pattern-blue">Ok, quero o meu pãozinho!</button>
									</div>

									<div class="form-messages"></div>

								</form>
							</div>

						</div>
					</div>
				</article>
			</section>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>
		<script type="text/javascript" src="<?php echo $path ?>/assets/js/honey.min.js"></script>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55c3ee06eb46d658" async="async"></script>

	</body>
</html>
