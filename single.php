<?php include_once('includes/metatag.php'); ?>

	<body id="single" itemscope itemtype="http://schema.org/WebPage">
		<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1618093858455395',
				xfbml      : true,
				version    : 'v2.4'
			});
		};

		(function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "//connect.facebook.net/pt_BR/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
		</script>
		<div id="fb-root"></div>

		<section class="all">
			<?php include_once('includes/header.php'); ?>

			<section id="all">
				<div class="normal-box">
					<div class="list-posts normal-box">

						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<article role="article" class="bigger normal" id="post" itemscope itemtype="http://schema.org/BlogPosting">
								<h1 itemprop="headline" class="title-chalk" role="banner"><?php the_title() ?></h1>

								<div class="author c-author-post">
									<?php
										$author_id=$post->post_author;
									?>

									<figure>
										<?php echo get_avatar( $author_id, 40 ); /* id, resolution/width */ ?>
									</figure>

									<div class="info">
										<h2>Por <?php the_author_posts_link(); ?></h2>
										<span itemprop="dateCreated"><?php the_time('d') ?> &#149; <?php the_time('m') ?> &#149; <?php the_time('Y') ?> </span>
									</div>

								</div>

								<div class="post-content" itemprop="text" role="description">

									<?php the_content() ?>

									<!-- end content -->

									<div class="fb-like"
										data-href="<?php the_permalink() ?>"
										data-layout="button_count"
										data-action="like"
										data-show-faces="true"
										data-share="false">
									</div>

								</div>

								<div class="normal box-sep-big">
									<span class="separator-big"></span>
								</div>

								<div class="normal author">
									<?php
										$author_id=$post->post_author;
									?>

									<section role="article" itemscope itemtype="http://schema.org/Person">
										<div class="thumb">
											<figure itemprop="image">
												<?php echo get_avatar( $author_id, 175 ); /* id, resolution/width */ ?>
											</figure>
										</div>

										<div class="info c-author-info">
											<h1 class="title-chalk" itemprop="author"><?php the_author_posts_link(); ?></h1>
											<h2 itemprop="description"><?php the_author_meta('description', $author_id); ?></h2>

											<ul class="socials" role="navigation">
												<li>
													<a itemprop="sameAs" href="<?php echo $twitter_url ?>" class="ico ico-tt" title="Oitavo Andar no Twitter" target="_blank"></a>
												</li>

												<li>
													<a itemprop="sameAs" href="<?php echo $facebook_url ?>" class="ico ico-fb" title="Oitavo Andar no Facebook" target="_blank"></a>
												</li>

												<li>
													<a itemprop="sameAs" href="<?php echo $instagram_url ?>" class="ico ico-instagram" title="Oitavo Andar no Instagram" target="_blank"></a>
												</li>

												<li>
													<a itemprop="sameAs" href="<?php echo $youtube_url ?>" class="ico ico-yt" title="Oitavo Andar no YouTube" target="_blank"></a>
												</li>
											</ul>

										</div>
									</section>
								</div> <!-- author -->

								<?php include_once('includes/related.php'); ?>

								<section class="comments">
									<h3 class="title-coalhand simple-title">O que achou? Deixe o seu comentário...</h3>

									<!-- fb comments - USE PERMALINK -->
									<div class="fb-comments"
										data-href="<?php the_permalink() ?>"
										data-numposts="5">
									</div>

									<!-- WordPress Comments -->
									<?php comments_template(); ?>

									<!-- WP COMMENT FORM SUBSTITUTE IT -->


								</section>


							</article>

						<?php endwhile; else: ?>
							<h2>Desculpe, mas nada foi encontrado.</h2>
						<?php endif; ?>

					</div>

					<?php include_once('includes/sidebar.php'); ?>

				</div>

			</section>

			<?php include_once('includes/footer.php'); ?>
		</section>

		<?php include_once('includes/script.php') ?>
		<script type="text/javascript" src="<?php echo $path; ?>/assets/js/single.min.js"></script>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55c3ee06eb46d658" async="async"></script>


	</body>
</html>
